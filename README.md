# Project Name 

Oddestodds

### Technolgies used

-This solution was built using Visual Studio 2017, using C# 7.1 (on two console apps) making use of .NET Core 2.2.
-The concept of this solution is that a Backend console application can make Restfull calls to a
Web Api project to manipulate data which is stored on Microsoft SQL Server Express.
A separate Frontend console application is used to consume data published by the Backend.

### Configuration and prerequisits

-.NET Core 2.2 
-C# 7.1 for a particular code feature in Oddestodds.BackOffice and Oddestodds.DatabaseSeeder
-RabbitMQ Server (+Erlang)
-SQL Server with and SQL Express instance
-Web Api will run on IIS Expres part of Visual Studio on URI http://localhost:6600/ which is configurable from projects .json file
-Note: By default, no configurations should be changed


### Solution

The solution is made up of 8 projects:

Database/Oddestodds.DatabaseSeeder
	-This needs to be run first on its own. This will create the database and insert some data.
Oddestodds.BackOffice (Needs to be Started)
	-Console application which allows the user to insert commands to modify data 
Oddestodds.Domain
	-A class library project that holds entity related classes and a structure for the Repository Pattern
Oddestodds.Domain.Impl
	-A class library project that holds model related classes which are used as data transfer  object from server to client
Oddestodds.FrontEnd (Needs to be Started)
	-A console application which consumes AMQP messages and displays real time data
Oddestodds.Infrastructure
	-A class library project that hold the DataContext, creates a database, and implements the Repository Pattern
Oddestodds.WebApi (Needs to be Started)
	-A Web Api project which is hosted on this URL http://localhost:6600/
Oddestodds.Services
	-A data logic layer which is used by the web api controller to get data and other logic

	
## Getting Started

Clone the project in Visual Studio. Look for the solution file name Oddestodds.sln
Make sure all Nuget packages are resolved
Set the console app named Oddestodds.DatabaseSeeder found in Database folder as start up project.
Edit the connection string found in appsettings.json which should point to an SQL Server instance. 
Note: If you change the connection string here you will also need to change it in the Oddestodds.WebApi project
Running this application will create the database and insert some test data. 
You will see 'Seeder complete.' when done.
	
## Installing

Change the start up projects to the following after executing the database console:
	-Oddestodds.WebApi
	-Oddestodds.BackOffice
	-Oddestodds.FrontEnd
Make sure the connection string in Oddestodds.WebApi is pointing to the newly created database
No other config setting should be changed. These include RabbitMQ Host name and Web API Url
Build and run the solution
Two console applications should start up, a Backoffice with a menu and a Frontend listening for Match Odds updates
Use the Backoffice console menu options to manage Match Odds.
	