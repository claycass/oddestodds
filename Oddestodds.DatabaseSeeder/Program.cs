﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Oddestodds.Infrastructure.Context;

namespace Oddestodds.DatabaseSeeder
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Oddestodds Database seeder");
            Console.WriteLine("Creating database if it does not exist..and inserting test data..");

            var config = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<OddestoddsContext>();
            optionsBuilder.UseSqlServer(config.GetConnectionString("OddestoddsConnectionString"));

            //Hardcoded instance for demo purposes.
            await Seeder.SeedAsync(new OddestoddsContext(optionsBuilder.Options), CancellationToken.None);

            Console.WriteLine("Seeder complete.");
            Console.ReadLine();
        }
    }
}
