﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Oddestodds.Domain.Entities;
using Oddestodds.Infrastructure.Context;

namespace Oddestodds.DatabaseSeeder
{
    public static class Seeder
    {
        public static async Task SeedAsync(OddestoddsContext context, CancellationToken cancellationToken)
        {
            await context.Database.EnsureCreatedAsync(cancellationToken);

            if (!await context.MatchOdds.AnyAsync(cancellationToken))
            {
                var matchOddList = new List<MatchOdd>
                {
                    new MatchOdd()
                    {
                        HomeTeamName = "Milan FC",
                        AwayTeamName = "Chelsea FC",
                        HomeWinOdd = 10,
                        AwayWinOdd = 20,
                        DrawOdd = 30
                    },
                    new MatchOdd()
                    {
                        HomeTeamName = "Arsenal FC",
                        AwayTeamName = "Toronto FC",
                        HomeWinOdd = (decimal) 5.55,
                        AwayWinOdd = (decimal) 15.90,
                        DrawOdd = 8
                    },
                    new MatchOdd()
                    {
                        HomeTeamName = "Watford FC",
                        AwayTeamName = "Newcastle FC",
                        HomeWinOdd = 15,
                        AwayWinOdd = 19,
                        DrawOdd = (decimal) 8.50
                    },
                    new MatchOdd()
                    {
                        HomeTeamName = "Liverpool FC",
                        AwayTeamName = "Everton FC",
                        HomeWinOdd = 25,
                        AwayWinOdd = 30,
                        DrawOdd = 35
                    }
                };

                await context.MatchOdds.AddRangeAsync(matchOddList, cancellationToken);
                await context.SaveChangesAsync(cancellationToken);
            }
        }
    }

}
