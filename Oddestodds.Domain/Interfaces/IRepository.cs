﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Oddestodds.Domain.Interfaces
{
    public interface IRepository<T>
    {
        Task<IEnumerable<T>> GetListAsync(CancellationToken cancellationToken);
        Task<T> AddAsync(T entity, CancellationToken cancellationToken);
        Task DeleteAsync(int id, CancellationToken cancellationToken);
        Task<T> UpdateAsync(T entity, CancellationToken cancellationToken);
        Task<T> FindByIdAsync(int id, CancellationToken cancellationToken);
    }
}
