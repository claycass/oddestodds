﻿using System.ComponentModel.DataAnnotations;

namespace Oddestodds.Domain.Entities
{
    public class MatchOdd
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string HomeTeamName { get; set; }

        [Required]
        [MaxLength(200)]
        public string AwayTeamName { get; set; }

        [Required]
        public decimal HomeWinOdd { get; set; }

        [Required]
        public decimal AwayWinOdd { get; set; }

        [Required]
        public decimal DrawOdd { get; set; }
    }
}
