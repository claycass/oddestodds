﻿using Oddestodds.Domain.Interfaces;

namespace Oddestodds.Domain.Entities
{
    public interface IMatchOddRepository : IRepository<MatchOdd>
    {
    }
}