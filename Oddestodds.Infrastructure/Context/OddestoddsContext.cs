﻿using Microsoft.EntityFrameworkCore;
using Oddestodds.Domain.Entities;

namespace Oddestodds.Infrastructure.Context
{
    public class OddestoddsContext : DbContext
    {
        public OddestoddsContext(DbContextOptions<OddestoddsContext> contextOptions) : base(contextOptions)
        {
        }

        public DbSet<MatchOdd> MatchOdds { get; set; }
    }
}