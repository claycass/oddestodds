﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Oddestodds.Domain.Interfaces;

namespace Oddestodds.Infrastructure.Repositories
{
    public class BaseRepository<TContext, T> : IRepository<T>
        where TContext : DbContext
        where T : class
    {
        private readonly TContext _context;

        public BaseRepository(TContext context)
        {
            _context = context;
        }

        public virtual async Task<IEnumerable<T>> GetListAsync(CancellationToken cancellationToken)
        {
            return await _context.Set<T>().ToListAsync(cancellationToken);
        }

        public virtual async Task<T> AddAsync(T entity, CancellationToken cancellationToken)
        {
            _context.Set<T>().Add(entity);
            await _context.SaveChangesAsync(cancellationToken);

            return entity;
        }

        public virtual async Task DeleteAsync(int id, CancellationToken cancellationToken)
        {
            var entity = await FindByIdAsync(id, cancellationToken);

            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task<T> UpdateAsync(T entity, CancellationToken cancellationToken)
        {
            _context.Set<T>().Update(entity);
            await _context.SaveChangesAsync(cancellationToken);
            return entity;
        }

        public virtual async Task<T> FindByIdAsync(int id, CancellationToken cancellationToken)
        {
            var entity = await _context.Set<T>().FindAsync(id);
            if (entity == null)
                throw new ApplicationException("No content found");
            return entity;
        }
    }
}