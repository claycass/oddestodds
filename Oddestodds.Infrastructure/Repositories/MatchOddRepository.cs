﻿using Oddestodds.Domain.Entities;
using Oddestodds.Infrastructure.Context;

namespace Oddestodds.Infrastructure.Repositories
{
    public class MatchOddRepository : BaseRepository<OddestoddsContext, MatchOdd>, IMatchOddRepository
    {
        public MatchOddRepository(OddestoddsContext context) : base(context)
        {
        }
    }
}
