﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Oddestodds.Domain.Impl.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Oddestodds.FrontEnd
{
    class Program
    {
        private static IConfigurationRoot _config;
        static void Main(string[] args)
        {
            Console.WriteLine("Oddestodds Front End");

            _config = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            ConsumeOdds();
        }

        private static void ConsumeOdds()
        {
            var factory = new ConnectionFactory() { HostName = _config.GetValue<string>("RabbitMqHostName") };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "MatchOdds", type: "fanout");

                var queueName = channel.QueueDeclare().QueueName;
                channel.QueueBind(queue: queueName,
                    exchange: "MatchOdds",
                    routingKey: "");

                Console.WriteLine("Waiting for Odds..");

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var matchOdds = JsonConvert.DeserializeObject<IEnumerable<MatchOddModel>>(Encoding.UTF8.GetString(body));

                    foreach (var matchOdd in matchOdds)
                    {
                        Console.WriteLine(matchOdd.ToString());
                        Console.WriteLine();
                    }
                };
                channel.BasicConsume(queue: queueName,
                    autoAck: true,
                    consumer: consumer);

                Console.ReadLine();
            }
        }
    }
}
