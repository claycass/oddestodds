﻿namespace Oddestodds.Domain.Impl.Models
{
    public class MatchOddModel
    {
        public int Id { get; set; }
        public string HomeTeamName { get; set; }
        public string AwayTeamName { get; set; }
        public decimal HomeWinOdd { get; set; }
        public decimal AwayWinOdd { get; set; }
        public decimal DrawOdd { get; set; }

        public override string ToString()
        {
            return $"[Id:] {Id} \t{HomeTeamName}\t[vs]\t{AwayTeamName}\t[1:] {HomeWinOdd}\t [X:] {DrawOdd}\t [2:] {AwayWinOdd}";
        }
    }
}
