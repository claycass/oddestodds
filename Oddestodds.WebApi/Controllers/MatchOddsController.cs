﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Oddestodds.Domain.Impl.Models;
using Oddestodds.Services.Interfaces;

namespace Oddestodds.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchOddsController : ControllerBase
    {
        private readonly IMatchOddDataService _matchOddDataService;

        public MatchOddsController(IMatchOddDataService matchOddDataService)
        {
            _matchOddDataService = matchOddDataService;
        }

        [HttpGet]
        [Route("GetAllMatchOddsAsync")]
        public async Task<IList<MatchOddModel>> GetAllMatchOddsAsync(CancellationToken cancellationToken)
        {
            return (await _matchOddDataService.GetAllMatchOddsAsync(cancellationToken)).ToList();
        }

        [HttpGet]
        [Route("GetMatchOddByIdAsync")]
        public async Task<MatchOddModel> GetMatchOddByIdAsync(int id, CancellationToken cancellationToken)
        {
            return await _matchOddDataService.GetMatchOddByIdAsync(id, cancellationToken);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(int id, CancellationToken cancellationToken)
        {
            await _matchOddDataService.DeleteAsync(id, cancellationToken);
        }

        [HttpPost]
        [Route("AddAsync")]
        public async Task<MatchOddModel> AddAsync(MatchOddModel matchOddModel, CancellationToken cancellationToken)
        {
            return await _matchOddDataService.AddAsync(matchOddModel, cancellationToken);
        }

        [HttpPut]
        public async Task<MatchOddModel> UpdateAsync(MatchOddModel matchOddModel, CancellationToken cancellationToken)
        {
            return await _matchOddDataService.UpdateAsync(matchOddModel, cancellationToken);
        }

        [HttpPost]
        [Route("PublishAsync")]
        public async Task PublishAsync(CancellationToken cancellationToken)
        {
            await _matchOddDataService.PublishAsync(cancellationToken);
        }
    }
}
