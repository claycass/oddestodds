﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Oddestodds.Domain.Entities;
using Oddestodds.Infrastructure.Context;
using Oddestodds.Infrastructure.Repositories;
using Oddestodds.Services.Interfaces;
using Oddestodds.Services.Services;
using Oddestodds.WebApi.Exceptions;

namespace Oddestodds.WebApi
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContextPool<OddestoddsContext>(opt => opt.UseSqlServer(_configuration.GetConnectionString("OddestoddsConnectionString")));

            services.AddScoped<IMatchOddRepository, MatchOddRepository>();
            services.AddScoped<IMatchOddDataService, MatchOddDataService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandlingMiddleware();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller}/{action}/{id?}");
            });
        }
    }
}
