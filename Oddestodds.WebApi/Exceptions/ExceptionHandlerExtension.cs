﻿using Microsoft.AspNetCore.Builder;

namespace Oddestodds.WebApi.Exceptions
{
    public static class ExceptionHandlerExtension
    {
        public static IApplicationBuilder UseExceptionHandlingMiddleware(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ExceptionHandlingMiddleware>();
        }
    }
}
