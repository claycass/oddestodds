﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Oddestodds.Domain.Impl.Models;

namespace Oddestodds.BackOffice.Interfaces
{
    public interface IMatchOddsService
    {
        Task<IEnumerable<MatchOddModel>> GetAllMatchOddsAsync(CancellationToken cancellationToken);
        Task<MatchOddModel> GetMatchOddByIdAsync(int id, CancellationToken cancellationToken);
        Task DeleteAsync(int id, CancellationToken cancellationToken);
        Task<MatchOddModel> AddAsync(MatchOddModel matchOddModel, CancellationToken cancellationToken);
        Task<MatchOddModel> UpdateAsync(MatchOddModel matchOddModel, CancellationToken cancellationToken);
        Task PublishAsync(CancellationToken cancellationToken);
    }
}
