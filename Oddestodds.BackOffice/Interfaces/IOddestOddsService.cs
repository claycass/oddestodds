﻿using System.Threading.Tasks;

namespace Oddestodds.BackOffice.Interfaces
{
    public interface IOddestOddsService
    {
        Task RunAsync();
    }
}
