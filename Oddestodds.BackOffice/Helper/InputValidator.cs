﻿using System;

namespace Oddestodds.BackOffice.Helper
{
    public static class InputValidator
    {
        public static T RequestReplacementValue<T>(string label, T previousValue)
        {
            Console.WriteLine($"{label} { previousValue}");
            var newValue = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(newValue))
            {
                if (typeof(T) == typeof(string))
                {
                    return (T)Convert.ChangeType(newValue, typeof(T));
                }

                if (typeof(T) == typeof(decimal))
                {
                    decimal value;
                    while (!decimal.TryParse(newValue, out value) || value <= 0)
                    {
                        Console.WriteLine("Not a valid decimal, try again.");

                        newValue = Console.ReadLine();
                        if (string.IsNullOrWhiteSpace(newValue))
                            break;
                    }
                    if (value > 0)
                        return (T)Convert.ChangeType(value, typeof(T));
                } 
            }

            return previousValue;
        }
        public static T RequestValue<T>(string label)
        {
            Console.WriteLine(label);

            var input = Console.ReadLine();

            if (typeof(T) == typeof(int))
            {
                int value;
                while (!int.TryParse(input, out value) || value <= 0)
                {
                    Console.WriteLine("Not a valid integer, try again.");

                    input = Console.ReadLine();
                }

                return (T)Convert.ChangeType(value, typeof(T));
            }
            if (typeof(T) == typeof(decimal))
            {
                decimal value;
                while (!decimal.TryParse(input, out value) || value <= 0)
                {
                    Console.WriteLine("Not a valid decimal, try again.");

                    input = Console.ReadLine();
                }

                return (T)Convert.ChangeType(value, typeof(T));
            }
            if (typeof(T) == typeof(string))
            {
                while (string.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine("Not a valid string, try again.");

                    input = Console.ReadLine();
                }

                return (T)Convert.ChangeType(input, typeof(T));
            }

            return (T)Convert.ChangeType(null, typeof(T));
        }
    }
}
