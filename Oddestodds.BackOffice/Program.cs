﻿using System;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Oddestodds.BackOffice.Interfaces;
using Oddestodds.BackOffice.Services;
using Microsoft.Extensions.Configuration;

namespace Oddestodds.BackOffice
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //IOC container
            var serviceCollection = new ServiceCollection();

            ConfigureServices(serviceCollection);

            var serviceProvider = serviceCollection.BuildServiceProvider();

            // An overall catch all to log any exception for demo purposes 
            try
            {
                await serviceProvider.GetService<IOddestOddsService>().RunAsync();
            }
            catch (Exception generalException)
            {
                // log the exception
                var logger = serviceProvider.GetService<ILogger<Program>>();
                logger.LogError(generalException, "An exception happened while running OddestOdds Backoffice console");
                Console.WriteLine("Something went wrong. Please try again later.");
            }

        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            serviceCollection.AddLogging();

            serviceCollection.AddHttpClient<IMatchOddsService, MatchOddsService>(client =>
            {
                client.BaseAddress = new Uri(config.GetValue<string>("WebApiBaseAddress"));
                client.Timeout = new TimeSpan(0, 0, 30);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/JSON"));
            });

            serviceCollection.AddScoped<IOddestOddsService, OddestOddsService>();
        }
    }
}