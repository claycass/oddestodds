﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Oddestodds.BackOffice.Helper;
using Oddestodds.BackOffice.Interfaces;
using Oddestodds.Domain.Impl.Models;

namespace Oddestodds.BackOffice.Services
{
    public class OddestOddsService : IOddestOddsService
    {
        private readonly IMatchOddsService _matchOddsService;
        private readonly ILogger<OddestOddsService> _logger;

        public OddestOddsService(IMatchOddsService matchOddsService, ILogger<OddestOddsService> logger)
        {
            _matchOddsService = matchOddsService;
            _logger = logger;
        }

        public async Task RunAsync()
        {
            Console.WriteLine("Oddestodds Back Office Console Application");

            PrintMenu();

            await ActionRouterAsync();

            Console.ReadLine();
        }

        private async Task ActionRouterAsync()
        {
            var exitActionRouter = false;

            while (!exitActionRouter)
            {
                Console.WriteLine("Action:");
                var actionCode = Console.ReadLine();

                switch (actionCode)
                {
                    case "view":
                    {
                        await ViewAsync();
                        break;
                    }
                    case "delete":
                    {
                        await DeleteAsync();
                        break;
                    }
                    case "add":
                    {
                        await AddAsync();
                        break;
                    }
                    case "update":
                    {
                        await UpdateAsync();
                        break;
                    }
                    case "publish":
                    {
                        await PublishAsync();
                        break;
                    }
                    case "exit":
                    {
                        exitActionRouter = true;
                        Console.WriteLine("Press any key to exit...");
                        break;
                    }
                    default:
                    {
                        Console.WriteLine("Command not recognised");
                        PrintMenu();
                        break;
                    }
                }
            }
        }

        private static void PrintMenu()
        {
            Console.WriteLine("Menu:");
            Console.WriteLine("Type 'view' to view all");
            Console.WriteLine("Type 'add' to add");
            Console.WriteLine("Type 'update' to update");
            Console.WriteLine("Type 'delete' to delete");
            Console.WriteLine("Type 'publish' to publish all");
            Console.WriteLine("Type 'exit' to close application");
        }

        private async Task ViewAsync()
        {
            Console.WriteLine("Getting all Match Odds..");

            try
            {
                var matchOdds = await _matchOddsService.GetAllMatchOddsAsync(CancellationToken.None);

                foreach (var matchOdd in matchOdds)
                {
                    Console.WriteLine(matchOdd.ToString());
                    Console.WriteLine();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "ViewAsync");
                Console.WriteLine("Something went wrong. Data cannot be retrieved now,");
            }
        }

        private async Task DeleteAsync()
        {
            var id = InputValidator.RequestValue<int>("Insert Match Odd Id to delete:");

            Console.WriteLine($"Deleting Match Odd with Id: {id}..");

            try
            {
                await _matchOddsService.DeleteAsync(id, CancellationToken.None);
                Console.WriteLine($"Match Odd with Id: {id} deleted");
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeleteAsync");
                Console.WriteLine("Something went wrong. Make sure Id belongs to an exisitng Match Odd");
            }    
        }

        private async Task AddAsync()
        {
            var matchOddModel = new MatchOddModel
            {
                HomeTeamName = InputValidator.RequestValue<string>("Insert Home Team Name:"),
                HomeWinOdd = InputValidator.RequestValue<decimal>("Insert Home Team Odd:"),
                AwayTeamName = InputValidator.RequestValue<string>("Insert Away Team Name:"),
                AwayWinOdd = InputValidator.RequestValue<decimal>("Insert Away Team Odd:"),
                DrawOdd = InputValidator.RequestValue<decimal>("Insert Draw Odd:")
            };

            Console.WriteLine("Adding new Match Odd..");

            try
            {
                matchOddModel = await _matchOddsService.AddAsync(matchOddModel, CancellationToken.None);
                Console.WriteLine(matchOddModel.ToString());
                Console.WriteLine();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AddAsync");
                Console.WriteLine("Something went wrong. Make sure data inputted is correct");
            }        
        }

        private async Task UpdateAsync()
        {
            var id = InputValidator.RequestValue<int>("Insert Match Odd Id to update:");

            Console.WriteLine($"Getting Match Odd with Id: {id}..");

            try
            {
                var matchOddModel = await _matchOddsService.GetMatchOddByIdAsync(id, CancellationToken.None);
                Console.WriteLine(matchOddModel.ToString());
                Console.WriteLine();

                matchOddModel.HomeTeamName = InputValidator.RequestReplacementValue("Type new Home Team Name or hit <Enter> to keep:", matchOddModel.HomeTeamName);
                matchOddModel.AwayTeamName = InputValidator.RequestReplacementValue("Type new Away Team Name or hit <Enter> to keep:", matchOddModel.AwayTeamName);
                matchOddModel.HomeWinOdd = InputValidator.RequestReplacementValue("Type new Home Win Odd or hit <Enter> to keep:", matchOddModel.HomeWinOdd);
                matchOddModel.AwayWinOdd = InputValidator.RequestReplacementValue("Type new Away Win Odd or hit <Enter> to keep:", matchOddModel.AwayWinOdd);
                matchOddModel.DrawOdd = InputValidator.RequestReplacementValue("Type new Draw Odd or hit <Enter> to keep:", matchOddModel.DrawOdd);

                matchOddModel = await _matchOddsService.UpdateAsync(matchOddModel, CancellationToken.None);
                Console.WriteLine(matchOddModel.ToString());
                Console.WriteLine();
                Console.WriteLine("Update successful");
            }
            catch (Exception e)
            {
                _logger.LogError(e, "UpdateAsync");
                Console.WriteLine("Something went wrong. Make sure Id belongs to an exisitng Match Odd");
            }
        }

        private async Task PublishAsync()
        {
            try
            {
                await _matchOddsService.PublishAsync(CancellationToken.None);
                Console.WriteLine("Publish successful");
            }
            catch (Exception e)
            {
                _logger.LogError(e, "PublishAsync");
                Console.WriteLine("Something went wrong. Pubish might have failed.");
            }
        }
    }
}
