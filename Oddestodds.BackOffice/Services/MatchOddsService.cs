﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Oddestodds.BackOffice.Interfaces;
using Oddestodds.Domain.Impl.Models;

namespace Oddestodds.BackOffice.Services
{
    public class MatchOddsService : IMatchOddsService
    {
        private readonly HttpClient _httpClient;
        public MatchOddsService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<MatchOddModel>> GetAllMatchOddsAsync(CancellationToken cancellationToken)
        {
            var response = await _httpClient.GetAsync("api/MatchOdds/GetAllMatchOddsAsync", CancellationToken.None);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IEnumerable<MatchOddModel>>(content);
        }

        public async Task<MatchOddModel> GetMatchOddByIdAsync(int id, CancellationToken cancellationToken)
        {
            var response = await _httpClient.GetAsync($"api/MatchOdds/GetMatchOddByIdAsync?id={id}", CancellationToken.None);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<MatchOddModel>(content);
        }

        public async Task DeleteAsync(int id, CancellationToken cancellationToken)
        {
            var response = await _httpClient.DeleteAsync($"api/MatchOdds/{id}", CancellationToken.None);
            response.EnsureSuccessStatusCode();
        }

        public async Task<MatchOddModel> AddAsync(MatchOddModel matchOddModel, CancellationToken cancellationToken)
        {
            var response = await _httpClient.PostAsync("api/MatchOdds/AddAsync",
                new StringContent(
                    JsonConvert.SerializeObject(matchOddModel),
                    Encoding.UTF8,
                    "application/json"),
                CancellationToken.None);

            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<MatchOddModel>(content);
        }

        public async Task<MatchOddModel> UpdateAsync(MatchOddModel matchOddModel, CancellationToken cancellationToken)
        {
            var response = await _httpClient.PutAsync("api/MatchOdds/",
                new StringContent(
                    JsonConvert.SerializeObject(matchOddModel),
                    Encoding.UTF8,
                    "application/json"),
                CancellationToken.None);

            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<MatchOddModel>(content);
        }

        public async Task PublishAsync(CancellationToken cancellationToken)
        {
            var response = await _httpClient.PostAsync("api/MatchOdds/PublishAsync", null, CancellationToken.None);

            response.EnsureSuccessStatusCode();
        }
    }
}
