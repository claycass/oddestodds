﻿using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Oddestodds.Domain.Entities;
using Oddestodds.Domain.Impl.Models;
using Oddestodds.Services.Interfaces;
using RabbitMQ.Client;

namespace Oddestodds.Services.Services
{
    public class MatchOddDataService : IMatchOddDataService
    {
        private readonly IMatchOddRepository _matchOddRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public MatchOddDataService(IMatchOddRepository matchOddRepository, IMapper mapper, IConfiguration config)
        {
            _matchOddRepository = matchOddRepository;
            _mapper = mapper;
            _config = config;
        }

        public async Task<IEnumerable<MatchOddModel>> GetAllMatchOddsAsync(CancellationToken cancellationToken)
        {
            return _mapper.Map<IList<MatchOddModel>>(await _matchOddRepository.GetListAsync(cancellationToken));
        }

        public async Task<MatchOddModel> GetMatchOddByIdAsync(int id, CancellationToken cancellationToken)
        {
            return _mapper.Map<MatchOddModel>(await _matchOddRepository.FindByIdAsync(id, cancellationToken));
        }

        public async Task DeleteAsync(int id, CancellationToken cancellationToken)
        {
            await _matchOddRepository.DeleteAsync(id, cancellationToken);
        }

        public async Task<MatchOddModel> AddAsync(MatchOddModel matchOddModel, CancellationToken cancellationToken)
        {
            return _mapper.Map<MatchOddModel>(await _matchOddRepository.AddAsync(_mapper.Map<MatchOdd>(matchOddModel), cancellationToken));
        }

        public async Task<MatchOddModel> UpdateAsync(MatchOddModel matchOddModel, CancellationToken cancellationToken)
        {
            return _mapper.Map<MatchOddModel>(await _matchOddRepository.UpdateAsync(_mapper.Map<MatchOdd>(matchOddModel), cancellationToken));
        }

        public async Task PublishAsync(CancellationToken cancellationToken)
        {
            var matchOdds = await _matchOddRepository.GetListAsync(cancellationToken);

            var factory = new ConnectionFactory() { HostName = _config.GetValue<string>("RabbitMqHostName") };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "MatchOdds", type: "fanout");

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(matchOdds));

                channel.BasicPublish(exchange: "MatchOdds",
                    routingKey: "",
                    basicProperties: null,
                    body: body);

            }
        }
    }
}
